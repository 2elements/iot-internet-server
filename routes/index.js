let express = require('express');
let router = express.Router();
let response = require('../config/response');
const {CODES} = require('../config/codes');
let {Node, ObjectId} = require('../models/node');


/* GET home page. */
router.get('/', function(req, res, next) {
    res.send(req.session);
});



module.exports = router;
