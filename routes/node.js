let express = require('express');
let router = express.Router();

let response = require('../config/response');
const {CODES} = require('../config/codes');
let {Node, ObjectId} = require('../models/node');
let auth = require('../middlewares/auth');

/* GET users listing. */
router.route('/register')
    .post(function (req, res, next) {
        console.log(req.body);
        let node = new Node();


        if (req.body.name && req.body.unique_id) {
            node.name = req.body.name;
            node.nodeId = req.body.unique_id;
            node.devices = [];

            for (let i = 1; i <= parseInt(req.body.total_devices); i++) {
                node.devices.push({id: i, state: 0});
            }

            Node.find({nodeId: req.body.unique_id}, function (err, docs) {
                if (docs.length) {
                    res.status(400).json(response(CODES.exists));
                } else {
                    node.save().then(function (data) {
                        res.json(response(CODES.success));
                    }, function (err) {
                        res.status(500).json(response(CODES.inError, err));
                    });
                }
            });


        } else {
            res.send(response(CODES.insufficientInfo, req.body));
        }

    });


router.route('/:uniqueId')
    .get(function (req, res, next) {
        console.log(req.params);

        if (!req.params.uniqueId) {
            res.send(response(CODES.insufficientInfo, req.body));
        } else {

            Node.findOne({
                nodeId: req.params.uniqueId
            }, 'name nodeId devices', function (err, docs) {
                if (err) {
                    res.status(500).send(response(CODES.inError));
                }
                else {
                    if (docs) {
                        res.send(response(CODES.success, docs));
                        Node.update({nodeId: req.params.uniqueId}, {$set: {lastSeen: Date.now()}}, function (err, docs) {

                            if (err) {
                                console.error("Error while updating last seen of node.");
                            }
                            else {
                                if (docs) {
                                    console.log("Last seen updated...");
                                } else {
                                    console.error("Error while updating last seen of node.");
                                }
                            }

                        });

                    } else {
                        res.status(400).send(response(CODES.notReg));
                    }
                }
            });
        }
    });

router.route('/state')
    .post(auth, function (req, res, next) {
        if (!req.body.nodeId || !req.body.deviceId || !req.body.state) {
            res.send(response(CODES.insufficientInfo, req.body));
        } else {
            Node.update({
                nodeId: req.body.nodeId,
                "devices.id": req.body.deviceId
            }, {$set: {"devices.$.state": req.body.state}}, function (err, docs) {

                if (err) {
                    res.status(500).send(response(CODES.inError));
                    //console.error("Error while updating last seen of node.");
                }
                else {
                    if (docs) {
                        //console.log("Last seen updated...");
                        res.send(response(CODES.success, docs));
                    } else {
                        res.status(500).send(response(CODES.inError));
                        //console.error("Error while updating last seen of node.");
                    }
                }

            });
        }
    });


router.route('/state/online')
    .get(function (req, res, next) {
        //res.render('index', { title: 'Express' });
        Node.find({
            lastSeen: {$gt: new Date((Date.now()) - 1000 * 10)}
        }, 'name nodeId devices', function (err, docs) {
            if (err) {
                res.status(500).send(response(CODES.inError));
            }
            else {
                if (docs) {
                    let nodes = [];
                    for (let i = 0; i < docs.length; i++) {
                        //console.log(docs[i]);
                        nodes.push(docs[i]['nodeId']);
                    }
                    res.send(response(CODES.success, nodes));
                } else {
                    res.send(response(CODES.success));
                }
            }
        });
        //res.send(req.session);
    });


module.exports = router;