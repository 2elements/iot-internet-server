let express = require('express');
let router = express.Router();
let path = require('path');
let response = require('../config/response');
const {CODES} = require('../config/codes');

/* GET home page. */
router.get('/:deviceId', function(req, res, next) {
  //res.render('index', { title: 'Express' });
    console.log(req.params, req.query);

    if(!req.query.version) {
        res.status(500).send(response(CODES.insufficientInfo, req.body));
    }
    let version = parseInt(req.query.version);
    console.log(version);
    res.sendFile(path.join(__dirname + '/../updates/' + (version+1) + '.bin'));
    //res.send("");

});

module.exports = router;
