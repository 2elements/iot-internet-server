let express = require('express');
let router = express.Router();

let response = require('../config/response');
const {CODES} = require('../config/codes');
let {Location, ObjectId} = require('../models/location');
let {Device, ObjectId2} = require('../models/devices');
let auth = require('../middlewares/auth');

/* GET users listing. */
router.route('/')
    .post(function (req, res, next) {
        console.log('req', req.body);
        try {
            if (req.body.username === "aditya" && req.body.password === "Aditya@123") {

                req.session.login({"id": "1", "username": req.body.username}, function (err) {
                    if (err) {
                        return res.status(500).send(CODES.inError, req.body, err.toString());
                    } else {
                        res.send(response(CODES.success, req.body));
                    }
                });
            }
            else
                res.send(response(CODES.login));
        } catch (err) {
            res.status(500).send(response(CODES.inError, req.body, err.toString()));
        }


    });

router.route('/locations')
    .get(auth, function (req, res, next) {
        console.log('req', req.body);
        try {
            Location.find({createdBy: req.session.userInfo.id}, function (err, data) {
                if (err) {
                    console.log(err);
                    res.status(400).json(response(CODES.error, err));
                }
                else {
                    console.log(data);
                    res.json(response(CODES.success, data));
                }
            });

        } catch (err) {
            res.status(500).send(response(CODES.inError, req.body, err.toString()));
        }
    })
    .post(auth, function (req, res, next) {
        console.log("Body", req.body);
        if(!req.body.name){
            res.send(response(CODES.nameRq));
        }
        else {
            try {
                let location = new Location();
                location.name = req.body.name;
                location.createdBy = req.session.userInfo.id;

                location.save().then(function (data) {
                    res.json(response(CODES.success, location));
                }, function (err) {
                    res.status(500).json(response(CODES.inError, err));
                });

            } catch (err) {
                res.status(500).send(response(CODES.inError, req.body, err.toString()));
            }
        }
    });

router.route('/devices')
    .get(auth, function(req, res, next){
        console.log('req', req.params);
        try {
            Device.find({}, function (err, data) {
                if (err) {
                    console.log(err);
                    res.status(400).json(response(CODES.error, err));
                }
                else {
                    console.log(data);
                    res.json(response(CODES.success, data));
                }
            });

        } catch (err) {
            res.status(500).send(response(CODES.inError, req.body, err.toString()));
        }
    })
    .post(auth, function(req, res, next){
        console.log("Body", req.body);
        if(!req.body.name || !req.body.nodeId || !req.body.deviceId || !req.body.locationId){
            res.send(response(CODES.insufficientInfo));
        }
        else {
            try {
                let device = new Device();
                device.name = req.body.name;
                device.createdBy = req.session.userInfo.id;

                device.nodeId = req.body.nodeId;
                device.deviceId = req.body.deviceId;
                device.locationId = req.body.locationId;

                device.save().then(function (data) {
                    res.json(response(CODES.success, device));
                }, function (err) {
                    res.status(500).json(response(CODES.inError, err));
                });

            } catch (err) {
                res.status(500).send(response(CODES.inError, req.body, err.toString()));
            }
        }
    });

module.exports = router;
