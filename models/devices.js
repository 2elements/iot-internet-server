let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let DeviceSchema = new Schema({
    name: String,
    nodeId: String,
    deviceId: Number,
    locationId: String,
    createdBy: String,
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.Device = mongoose.model('Device', DeviceSchema);
module.exports.ObjectId = ObjectId;