let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let LocationSchema = new Schema({
    name: String,
    createdBy: String,
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.Location = mongoose.model('Location', LocationSchema);
module.exports.ObjectId = ObjectId;