/**
 * Created by aditya on 05/03/17.
 */

let mongoose = require('../config/database');
let ObjectId = require('../config/database').Types.ObjectId;
let Schema = mongoose.Schema;

let NodeSchema = new Schema({
    name: String,
    nodeId: String,
    devices: [{
        id: Number,
        state: Number
    }],
    lastSeen: {type: Date, default: Date.now()},
    dateCreated: {type: Date, default: Date.now()}
});

module.exports.Node = mongoose.model('Node', NodeSchema);
module.exports.ObjectId = ObjectId;
