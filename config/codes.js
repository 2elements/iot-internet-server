/**
 * Created by aditya on 05/03/17.
 */

const CODES = {
    success: {code: 1, msg: "Success"},
    error: {code: 0, msg: "Error occurred"},
    session: {code: -99, msg: "User not logged In"},
    login: {code: -100, msg: "Invalid Email or Password"},
    nameRq: {code: -101, msg: "Name is required"},
    emailRq: {code: -102, msg: "Email is required"},
    passRq: {code: -103, msg: "Password is required"},
    passLen: {code: -104, msg: "Password min. length is 6 required"},
    passMisM: {code: -105, msg: "Password and Confirm Password do not match"},
    emailReg: {code: -106, msg: "Email is already registered"},
    accNActive: {code: -107, msg: "Account not activated. Please check your inbox."},
    exists: {code: -108, msg: "Already exists"},
    upload: {code: -109, msg: "Upload error"},
    uploadImages: {code: -110, msg: "Only images are allowed"},
    selectImage: {code: -111, msg: "Select image to upload"},
    categoryInvalid: {code: -112, msg: "Invalid category"},
    menuItemInvalid: {code: -113, msg: "Invalid Menu Item"},
    insufficientInfo: {code: -114, msg: "Info not complete"},
    notReg: {code: -115, msg: "Not Registered"},
    landmarkRq: {code: -115, msg: "Landmark is required"},
    phoneRq: {code: -115, msg: "Phone Number is required"},
    inError: {code: -500, msg: "Internal Server Error"},

};

const getRqCODE = function (msg, code = -200) {
    return {code: code, msg: msg + ' is required'};
};


module.exports.CODES = CODES;
module.exports.getRqCODE = getRqCODE;