/**
 * Created by aditya on 05/03/17.
 */

let response = function(code_msg, data = {}, extras = {})
{
    return {
        code: code_msg.code,
        message: code_msg.msg,
        data: data,
        extras: extras
    };

};


module.exports = response;