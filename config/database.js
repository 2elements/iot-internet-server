/**
 * Created by aditya on 05/03/17.
 */
let app = require('../app');

let mongoose = require('mongoose');
// Use bluebird
mongoose.Promise = require('bluebird');

console.log("env",app.get('env'));

try {
    if(process.env['MONGO'])
        mongoose.connect('mongodb://@database:27017/IOT'); // connect to Docker database
    else
        mongoose.connect('mongodb://@127.0.0.1:27017/IOT'); // connect to Local database

} catch (err) {
    console.log(err);
}


module.exports = mongoose;