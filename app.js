let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let cors = require('cors');
let compression = require('compression');


//for security
let helmet = require('helmet');
let session = require('express-session');

let app = module.exports = express();

require('./config/database');

let indexRoute = require('./routes/index');
let userRoute = require('./routes/users');
let nodeRoute = require('./routes/node');
let updatesRoute = require('./routes/updates');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//for security
app.use(compression());
app.use(helmet());
app.disable('x-powered-by');
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        maxAge: 1000 * 60 * 24 * 7 // 24*7 hours
    },
    name : "session-id"
}));

session.Session.prototype.login = function (user, cb) {
    const req = this.req;
    req.session.regenerate(function(err){
        if (err){
            cb(err);
        }
    });
    req.session.userInfo = user;
    cb();
};


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRoute);
app.use('/users', userRoute);
app.use('/node', nodeRoute);
app.use('/updates', updatesRoute);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
