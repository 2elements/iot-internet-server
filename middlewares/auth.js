/**
 * Created by aditya on 05/03/17.
 */

let response = require('../config/response');
const {CODES} = require('../config/codes');

let auth = function (req, res, next) {
    // Authentication and Authorization Middleware
    if (req.session && req.session.userInfo) {
        if (req.session.userInfo.id)
            return next();
    }

        //return res.status(401).send(response(CODES.session, req.cookies.adminSecurityKey, req.session.adminSecurityKey));
        return res.status(401).send(response(CODES.session, req.session.admin));
};


module.exports = auth;